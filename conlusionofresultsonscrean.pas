unit ConlusionOfResultsOnScrean;

interface
uses OperationsOnElementsOfMatrix;
const
  SizeOfMatrix=5;

type
   matr=array[1..SizeOfMatrix,1..SizeOfMatrix] of real;

procedure PrintMatrix(Matrix:matr);
procedure PrintMinimumAndHisPositions(HorisontalPosition,VerticalPosition:integer; Minimum:real);
procedure PrintArifmeticalMean(Matrix:matr);

implementation

procedure PrintMatrix(Matrix:matr);
var
  i,j:integer;
begin
  writeln;
  for i:=1 to SizeOfMatrix do
  begin
  for j:=1 to SizeOfMatrix do
    write(Matrix[i,j]:7:2);
  writeln;
  end;
end;

procedure PrintMinimumAndHisPositions(HorisontalPosition,VerticalPosition:integer; Minimum:real);
begin
   writeln;
   writeln('MinimumElementsOfMatrix = ',Minimum:0:2);
   writeln('AndItsPositions :',HorisontalPosition,'  ',VerticalPosition);
   writeln;
end;

procedure PrintArifmeticalMean(Matrix:matr);
begin
  writeln;
  writeln('ArifmeticalMean = ',FindArifmeticalMean(Matrix):0:2);
  writeln;
end;

end.
