
uses OperationsOnElementsOfMatrix, ConlusionOfResultsOnScrean;

var
  Matrix:matr;
  HorisontalPosition,VerticalPosition:integer;
  Minimum:real;

begin
  Randomize;
  CreateMatrix(Matrix);
  PrintMatrix(Matrix);
  FindMinimumElementOfMatrix(Matrix,HorisontalPosition,VerticalPosition,Minimum);
  PrintMinimumAndHisPositions(HorisontalPosition,VerticalPosition,Minimum);
  PrintArifmeticalMean(Matrix);
  ReplaceMinimumOnArifmeticalMean(Matrix,HorisontalPosition,VerticalPosition);
  PrintMatrix(Matrix);
  readln;
end.

