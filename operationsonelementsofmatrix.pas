unit OperationsOnElementsOfMatrix;

interface

const
  SizeOfMatrix=5;
  EndRange =5;
  BeginRange=-7;

type
   matr=array[1..SizeOfMatrix,1..SizeOfMatrix] of real;

procedure CreateMatrix(var Matrix:matr);
procedure FindMinimumElementOfMatrix(Matrix:matr; var HorisontalPosition,VerticalPosition:integer;Var Minimum:real);
function TestLocationElementUnderSideDiagonal (i,j:integer):boolean;
function FindArifmeticalMean(Matrix:matr):real;
procedure ReplaceMinimumOnArifmeticalMean (var Matrix:matr; HorisontalPosition,VerticalPosition:integer);

implementation

procedure CreateMatrix(var Matrix:matr);
var
  i,j:integer;
begin
  for i:=1 to SizeOfMatrix do
    begin
    for j:=1 to SizeOfMatrix do
      Matrix[i,j]:=random(EndRange-BeginRange+1)+BeginRange;
    end;
end;

procedure FindMinimumElementOfMatrix(Matrix:matr; Var HorisontalPosition,VerticalPosition:integer; Var Minimum:real);
var
  i,j:integer;
begin
  Minimum:=Matrix[1,1];
  HorisontalPosition:=1;
  VerticalPosition:=1;
  for i:=1 to SizeOfMatrix do
    for j:=1 to SizeOfMatrix do
    begin
      if (i=j) and (Minimum>Matrix[i,j]) then
      begin
        Minimum:=Matrix[i,j];
        HorisontalPosition:=i;
        VerticalPosition:=j;
      end;
    end;
end;

function TestLocationElementUnderSideDiagonal(i,j:integer):boolean;
begin
    if ((i+j)>(SizeOfMatrix+1)) then
      TestLocationElementUnderSideDiagonal:=true
    else TestLocationElementUnderSideDiagonal:=false;
end;

function FindArifmeticalMean(Matrix:matr):real;
var
  i,j,NumberOfElements:integer;
  Sum:real;
begin
  NumberOfElements:=0;
  Sum:=0;
  for i:=1 to SizeOfMatrix do
    for j:=1 to SizeOfMatrix do
    begin
      if TestLocationElementUnderSideDiagonal(i,j) then
      begin
        Sum:=Sum+Matrix[i,j];
        NumberOfElements:=NumberOfElements+1;
      end;
    end;
   FindArifmeticalMean:=Sum/NumberOfElements;
end;

procedure ReplaceMinimumOnArifmeticalMean (Var Matrix:matr; HorisontalPosition,VerticalPosition:integer);
begin
  Matrix[HorisontalPosition,VerticalPosition]:=FindArifmeticalMean(Matrix);
end;

end.
